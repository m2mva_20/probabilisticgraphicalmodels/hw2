#!/bin/python

import os
import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
from imageio import imread
from numba import njit
from tqdm import tqdm

warnings.filterwarnings('error')

try:
    from utils import blur_binary_image, load_binary_image
except:
    from Code.utils import blur_binary_image, load_binary_image

@njit
def blur_img(img, u0, u1, std):
    n,m = img.shape
    noisy_img = np.zeros((n,m))
    for i in range(n):
        for j in range(m):
            e_ij = rd.randn()
            noisy_img[i,j] = img[i,j]*(u0 + std*e_ij) + (1.-img[i,j])*(u1 + std*e_ij)
            # thresholding so that the noisy img remains binary
            if noisy_img[i,j] > 0.5: noisy_img[i,j] = 1.
            else : noisy_img[i,j] = 0.
    return noisy_img

@njit
def get_neighbours(i, nCols, nRows):
        N = np.ones((4))*(-1)
        if i % nCols != 0:
            N[0]=(i-1)
        if (i+1)%(nCols)!=0 and i!=nCols*nRows-1 or i==0:
            N[1]=(i+1)
        if i>=nCols:
            N[2]=(i-nCols)
        if i<nCols*nRows-nCols:
            N[3]=(i+nCols)
        return N

@njit
def p_joint(i, label, X, Y, a, b, u0, u1):
    p = 1
    U = [u0, u1]
    N = get_neighbours(i, X.shape[0], X.shape[1])
    for j in range(4):
        if int(N[j]) != -1 and label == X.ravel()[int(N[j])]:
            p*=np.exp(b)
    p *= np.exp(-.5*(Y.ravel()[i]-U[label])**2)*np.exp(a*label)
    return p

@njit
def p_joint_normalized(i, label, X, Y, a, b, u0, u1):
    s = 0
    for l in range(2):
        s += p_joint(i,l,X,Y,a,b, u0, u1)
    return p_joint(i, label, X, Y, a, b, u0, u1)/s

@njit
def gibbs_sampler(n_iter, alpha, beta, Y, X, u0, u1):
    for iteration in range(n_iter):
        for i in range(Y.size):
            if rd.uniform(0,1)<p_joint_normalized(i, 0, X, Y, alpha, beta, u0, u1) :
                X.ravel()[i] = 0
            else:
                X.ravel()[i] = 1
    return X

def main():
    alpha  = .1
    beta   = .5
    n_iter =  100
    Y      = blur_img(load_binary_image('../Data/spiral.png'), 0, 1, 1).astype('int')
    X      = np.ones(Y.shape, dtype='int')
    u0     = 0
    u1     = 1
    X_est  = gibbs_sampler(n_iter, alpha, beta, Y, X, u0, u1)
    plt.imshow(X_est)
    plt.show()

if __name__=='__main__':
    main()
