#!/bin/python

import os
import warnings

import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rd
from imageio import imread
from tqdm import tqdm

warnings.filterwarnings('error')

try:
    from utils import blur_binary_image, load_binary_image
except:
    from Code.utils import blur_binary_image, load_binary_image

class LBP:

    def __init__(self, dataPath, parameters, n_iter=-1):
        '''
        dataPath   = Path to a binary image
        parameters = Dictionary in the form {alpha : float, beta : float, nu : [float, float]}
        n_iter     = Number of iteration for the Loopy belief propagation algorithm
        '''
        self.alpha   = parameters['alpha']
        self.beta    = parameters['beta']
        self.nu      = parameters['nu']

        self.Y_true  = load_binary_image(dataPath)
        self.Y_noisy = blur_binary_image(self.Y_true)
        self.Y_est   = np.zeros(self.Y_true.shape)

        self.rows    = self.Y_true.shape[0]
        self.cols    = self.Y_true.shape[1]
        self.n       = self.rows*self.cols

        self.msg     = np.ones((self.n, 4, 2))
        self.n_msg   = self.msg.copy()
        self.n_iter  = n_iter
        self.labels  = [0,1]

        if self.n_iter>0:
            self.propage(self.n_iter)
            self.infer()

    def data_cost(self, i, l):
        if self.Y_noisy.ravel()[i]==l:
            return .8
        return .2

    def smoothness_cost(self, l1, l2):
        if l1==l2:
            return np.exp(self.alpha*l1 + self.beta)
        return np.exp(self.alpha*l1)

    def get_neighbours(self, i):
        N = []
        if i % self.cols != 0:
            N.append(i-1)
        if (i+1)%(self.cols)!=0 and i!=self.n-1 or i==0:
            N.append(i+1)
        if i>=self.cols:
            N.append(i-self.cols)
        if i<self.n-self.cols:
            N.append(i+self.cols)
        return N

    def prod_msg(self,j,l,ex =-1):
        ngh = self.get_neighbours(j)
        p   = 1
        for id_n,n in enumerate(ngh):
            if n != ex:
                p *= self.msg[n, self.get_neighbours(n).index(j), l]
        return p

    def msg_update(self,j,i,id_i,l):
        self.n_msg[j,id_i,l] = sum([ self.data_cost(j, l)*self.smoothness_cost(l,label)*self.prod_msg(j,label, ex = i) for label in self.labels])

    def msg_normalize(self,j,i):
        try:
            self.n_msg[j,i] = self.n_msg[j,i]/sum([self.n_msg[j,i,l] for l in self.labels])
        except:
            print(self.n_msg[j,i])

    def belief(self,i,label):
        return self.data_cost(i, label)*self.prod_msg(i, label)

    def propage(self, n_iter):
        for t in range(n_iter):
            for node in tqdm(range(self.n)):
                for id_neigh,neigh in enumerate(self.get_neighbours(node)):
                    for label in self.labels:
                        self.msg_update(node,neigh,id_neigh,label)
                    self.msg_normalize(node,id_neigh)
            self.msg = self.n_msg.copy()
            self.n_msg = np.ones((self.msg.shape))

    def infer(self):
        for r in range(self.rows):
            for c in range(self.cols):
                i = c + r*self.cols
                self.Y_est[r,c] = np.argmax([self.belief(i,l) for l in self.labels])
        print("Accuracy : {}".format(self.accuracy()))


    def accuracy(self):
        return 1- sum((abs(self.Y_est-self.Y_true)).ravel())/self.n

    def show_results(self, save_path='../Fig'):
        plt.figure(figsize=(20,10))
        plt.subplot(1,3,1)
        plt.imshow(self.Y_true, cmap='gray')
        plt.title('True image')
        plt.subplot(1,3,2)
        plt.imshow(self.Y_noisy, cmap='gray')
        plt.title('Noisy image')
        plt.subplot(1,3,3)
        plt.imshow(self.Y_est, cmap='gray')
        plt.title('Estimated image\n{} iterations of LBP\nAccuracy : {:.2f}'.format(self.n_iter, self.accuracy()))
        plt.savefig(save_path+'/LBP_results.pdf')
        plt.show()

def main():
    param = {}
    param['alpha'] = .1
    param['beta']  = .5
    param['nu']    = [.2,.8]
    a=LBP(dataPath='../Data/spiral.png', parameters=param, n_iter=1)
    a.show_results()

if __name__=="__main__":
    main()

