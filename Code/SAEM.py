import numpy as np
from numpy import random as rd

rd.seed(42)

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from numba import jit

try:
    from Gibbs import gibbs_sampler
    from utils import blur_binary_image, load_binary_image
except:
    from Code.Gibbs import gibbs_sampler
    from Code.utils import blur_binary_image, load_binary_image

@jit(nopython=True)
def blur_img(img, u0, u1, std):
    n,m = img.shape
    noisy_img = np.zeros((n,m))
    for i in range(n):
        for j in range(m):
            e_ij = rd.randn()
            noisy_img[i,j] = img[i,j]*(u0 + std*e_ij) + (1.-img[i,j])*(u1 + std*e_ij)
            # thresholding so that the noisy img remains binary
            if noisy_img[i,j] > 0.5: noisy_img[i,j] = 1.
            else : noisy_img[i,j] = 0.
    return noisy_img


@jit(nopython=True)
def exhaustive_statistics(X,Y):
    # computation of the exhaustive statistic
    S0 = np.sum(X)
    S1 = np.sum((1.-X)*Y)
    S2 = np.sum(X*Y)
    return np.array([S0, S1, S2])

@jit(nopython=True)
def MAP(n,m, S):
    u0 = S[1]/(n*m - S[0])
    u1 = S[2]/S[0]
    return u0, u1

@jit(nopython=True)
def SAEM(u0, u1, Y, nIter, nMCMC, alpha, beta):
    n,m = Y.shape
    Nb = int(nIter/2) # burn-in parameter
    # initialisation
    X = rd.choice(1, size=(n,m)) # X_ij = 0 or = 1 with p=0.5
    S = np.zeros(3)
    for k in range(nIter):
        ## E step
        # simulation of Xt according to p(Xt-1 | Y, theta_t-1)
        #X = gibbs_sampling_of_the_RMF(Y, X, u0, u1, nMCMC)
        X = gibbs_sampler(nMCMC, alpha, beta, Y, X, u0, u1)
        # Stochastic Approximation of the conditional esperance of log p(X,Y,theta)
        if k <= Nb: stepSize = 1.
        else: stepSize = (k-Nb)**(-0.55)
        S = (1.-stepSize)*S + stepSize*exhaustive_statistics(X,Y)
        # M step
        # theta_t from maximization qt(theta)    = <St|phi(theta)> + psi(theta)
        # (model belongs to the exponantial family) by MAP
        u0,u1 = MAP(n, m, S)
    return X, u0, u1

def main():
    alpha  = .1
    beta   = .5
    n_iter =  50
    Y      = blur_img(load_binary_image('../Data/spiral.png'), 0, 1, .2).astype('int')
    X      = np.ones(Y.shape, dtype='int')
    X_est, u0, u1  = SAEM(.5,.5, Y, 50, 50, alpha, beta)
    print(u0, u1)

if __name__=="__main__":
    main()
