#!/bin/python

import numpy as np
import numpy.random as rd
from imageio import imread


def load_binary_image(path):
    data = imread(path)
    if len(data.shape)==3:
        return (data[:,:,0]/255).astype('int')
    return (data/255).astype('int')


def blur_binary_image(x, blur_ratio = .2):
    n,m    = x.shape
    N      = int(blur_ratio*n*m)
    mask_x = rd.choice(list(range(n)), size=N)
    mask_y = rd.choice(list(range(m)), size=N)
    blur   = x.copy()
    blur[mask_x,mask_y] = (1 + blur[mask_x,mask_y])%2
    return blur

